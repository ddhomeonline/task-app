<div>
    
    <div class="col-md-6">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> Campo Invalido.<br><br>
            <ul style="list-style-type:none;">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{-- Esta la variable actualizar es true incluye formulario update de lo contrario el de create --}}
    @if($actualizar)
        @include('livewire.update')
    @else
        @include('livewire.create')
    @endif


    <table class="table table-striped" style="margin-top:20px;">
        <tr>
            <td>ID</td>
            <td>TITULO</td>
            <td>DESCRIPCION</td>
            <td>ACCIÓN</td>
        </tr>

        @foreach($tasks as $task)
            <tr>
                <td>{{$task->id}}</td>
                <td>{{$task->titulo}}</td>
                <td>{{$task->descripcion}}</td>
                <td>
                    <button wire:click="edit({{$task->id}})" class="btn btn-sm btn-outline-danger py-0">Editar</button> | 
                    <button wire:click="destroy({{$task->id}})" class="btn btn-sm btn-outline-danger py-0">Eliminar</button>
                </td>
            </tr>
        @endforeach
        {{ $tasks->links() }}
    </table>
    </div>

</div> 
