<div>
    <h1>Editar Tarea</h1>
    <input type="hidden" wire:model="selected_id">
    <div class="form-group">
        <label for="titulo">Titulo</label>
        <input type="text" wire:model="titulo" class="form-control input-sm"  placeholder="Ingrese Titulo">
    </div>
    <div class="form-group">
        <label>Descripción</label>
        <input type="text" class="form-control input-sm" placeholder="Ingrese descripción" wire:model="descripcion">
    </div>
    <button wire:click="update()" class="btn btn-primary">Actualizar</button>
</div> 