<div>
    <h1>Crear Tarea</h1>
    <div class="form-group">
        <label for="exampleInputPassword1">Ingrese Titulo</label>
        <input type="text" wire:model="titulo" class="form-control input-sm"  placeholder="Titulo de la tarea">
    </div>
    <div class="form-group">
        <label>Ingrese Descripción</label>
        <input type="text" class="form-control input-sm" placeholder="Ingrese Descripcion" wire:model="descripcion">
    </div>
    <button wire:click="store()" class="btn btn-primary">Crear Tarea</button>
</div>