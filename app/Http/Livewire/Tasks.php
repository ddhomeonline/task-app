<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Task;
use Livewire\WithPagination;

class Tasks extends Component
{
    use WithPagination;

    public $titulo, $descripcion, $selected_id;
    public $actualizar = false;

    public function render()
    {
        //La paginación se hace con 10 registros por página
        return view('livewire.task', [
            'tasks' => Task::paginate(10),
        ]);
    }

    //limpia los campos del formulario crear tarea
    private function resetInput()
    {
        $this->titulo = null;
        $this->descripcion = null;
    }
    public function store()
    {
        //Validación de los valores introducidos de una Tarea
        $this->validate([
            'titulo' => 'required|min:5',
            'descripcion' => 'required'
        ]);
        //Creación de la tarea de una Tarea
        Task::create([
            'titulo' => $this->titulo,
            'descripcion' => $this->descripcion
        ]);

        $this->resetInput();
    }
    public function edit($id)
    {
        $tarea = Task::findOrFail($id);
        $this->selected_id = $id;
        $this->titulo = $tarea->titulo;
        $this->descripcion = $tarea->descripcion;
        //cambio en el valor de esta varible a true para mostrar si es un formulario de actualizar
        $this->actualizar = true;
    }
    public function update()
    {
        $this->validate([
            'selected_id' => 'required|numeric',
            'titulo' => 'required|min:5',
            'descripcion' => 'required'
        ]);
        if ($this->selected_id) {
            $tarea = Task::find($this->selected_id);
            $tarea->update([
                'titulo' => $this->titulo,
                'descripcion' => $this->descripcion
            ]);
            $this->resetInput();
            //cambio en el valor de esta varible a false para mostrar si es un formulario de editar
            $this->actualizar = false;
        }
    }
    public function destroy($id)
    {
        if ($id) {
            $tarea = Task::where('id', $id);
            $tarea->delete();
        }
    }
}
